var NhanVien = function (
  taiKhoan,
  hoVaTen,
  email,
  matKhau,
  ngayLam,
  luongCoBan,
  chucVu,
  soGioLam
) {
  this.taiKhoan = taiKhoan;
  this.hoVaTen = hoVaTen;
  this.email = email;
  this.matKhau = matKhau;
  this.ngayLam = ngayLam;
  this.luongCoBan = luongCoBan;
  this.chucVu = chucVu;
  this.soGioLam = soGioLam;
};
NhanVien.prototype.tinhTongLuong = function () {
  if (this.chucVu == "Sếp") {
    return this.luongCoBan * 3;
  } else if (this.chucVu == "Trưởng phòng") {
    return this.luongCoBan * 2;
  } else if (this.chucVu == "Nhân viên") {
    return this.luongCoBan;
  }
};
NhanVien.prototype.xepLoai = function () {
  if (this.soGioLam >= 192) return "Nhân viên xuất sắc";
  else if (this.soGioLam >= 176) return "Nhân viên giỏi";
  else if (this.soGioLam >= 160) return "Nhân viên khá";
  else return "Nhân viên trung bình";
};
