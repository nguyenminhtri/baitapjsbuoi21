// danh sach nhan vien
var danhSachNhanVien = [];
var mangThongBao = [
  "Vui lòng nhập tài khoản",
  "Vui lòng nhập họ tên",
  "Vui lòng nhập email",
  "Vui lòng nhập mật khẩu",
  "Vui lòng chọn ngày làm",
  "Vui lòng nhập lương cơ bản",
  "Vui lòng chọn chức vụ",
  "Vui lòng nhập giờ làm",
  "Vui lòng chọn chức vụ",
  "Vui lòng nhập số ",
  "Vui lòng nhập kí tự",
  "Vui lòng nhập đúng định dạng vd: user@gmai.com",
  "Vui lòng nhập mật Khẩu từ 6-10 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt)",
  "Vui lòng nhập đúng định dạng mm/dd/yyyy",
  " Lương cơ bản 1 000 000 - 20 000 000",
  "Vui lòng nhập 80 - 200 giờ",
];
var mangId = [
  "tbTKNV",
  "tbTen",
  "tbEmail",
  "tbMatKhau",
  "tbNgay",
  "tbLuongCB",
  "tbGiolam",
  "tbChucVu",
];
// luu danh sach nhan vien vao localstorage
function luuLocalstorage() {
  var dataJson = JSON.stringify(danhSachNhanVien);

  localStorage.setItem("DSNV", dataJson);
}

var dsnv = localStorage.getItem("DSNV");
if (dsnv !== null) {
  var mangNhanVien = JSON.parse(dsnv);

  for (i = 0; i < mangNhanVien.length; i++) {
    var item = mangNhanVien[i];

    var nv = new NhanVien(
      item.taiKhoan,
      item.hoVaTen,
      item.email,
      item.matKhau,
      item.ngayLam,
      item.luongCoBan,
      item.chucVu,
      item.soGioLam
    );
    danhSachNhanVien.push(nv);
  }
  luuLocalstorage();
  renderDSNV(danhSachNhanVien);
}
//

//Them nhan vien
document.getElementById("btnThemNV").addEventListener("click", function () {
  var nhanVien = layThongTinTuForm();
  var isValid = true;
  // kiem tra tai khoan
  isValid =
    isValid &
    (kiemTraRong(nhanVien.taiKhoan, mangId[0], mangThongBao[0]) &&
      kiemTraDoDai(nhanVien.taiKhoan, mangId[0], 4, 6));
  //kiem tra ten
  isValid =
    isValid &
    (kiemTraRong(nhanVien.hoVaTen, mangId[1], mangThongBao[1]) &&
      kiemTraKiTuNhap(nhanVien.hoVaTen, regexKTT, mangId[1], mangThongBao[10]));
  // kiem tra email
  isValid =
    isValid &
    (kiemTraRong(nhanVien.email, mangId[2], mangThongBao[2]) &&
      kiemTraKiTuNhap(nhanVien.email, regexEmail, mangId[2], mangThongBao[11]));
  // kiem tra mat khau
  isValid =
    isValid &
    (kiemTraRong(nhanVien.matKhau, mangId[3], mangThongBao[3]) &&
      kiemTraKiTuNhap(
        nhanVien.matKhau,
        regexKTMK,
        mangId[3],
        mangThongBao[12]
      ));
  //kiem tra ngay
  isValid =
    isValid &
    (kiemTraRong(nhanVien.ngayLam, mangId[4], mangThongBao[4]) &&
      kiemTraKiTuNhap(
        nhanVien.ngayLam,
        regexKTNgay,
        mangId[4],
        mangThongBao[13]
      ));
  isValid =
    isValid &
    (kiemTraRong(nhanVien.luongCoBan, mangId[5], mangThongBao[5]) &&
      kiemTraKiTuNhap(
        nhanVien.luongCoBan,
        regexKTS,
        mangId[5],
        mangThongBao[9]
      ) &&
      kiemTraNhapSo(
        1000000,
        nhanVien.luongCoBan,
        mangId[5],
        mangThongBao[14],
        20000000
      ));
  isValid = isValid & kiemTraChucVu(mangId[7], mangThongBao[6]);
  isValid =
    isValid &
    (kiemTraRong(nhanVien.soGioLam, mangId[6], mangThongBao[7]) &&
      kiemTraKiTuNhap(
        nhanVien.soGioLam,
        regexKTS,
        mangId[6],
        mangThongBao[9]
      ) &&
      kiemTraNhapSo(80, nhanVien.soGioLam, mangId[6], mangThongBao[15], 200));
  if (isValid) {
    danhSachNhanVien.push(nhanVien);
    luuLocalstorage();
    renderDSNV(danhSachNhanVien);
    resetForm();
  }
});
function xoaNv(id) {
  var viTri = timViTri(id, danhSachNhanVien);

  danhSachNhanVien.splice(viTri, 1);
  luuLocalstorage();
  renderDSNV(danhSachNhanVien);
}
function SuaNv(id) {
  var viTri = timViTri(id, danhSachNhanVien);
  var data = danhSachNhanVien[viTri];
  showThongTinLenForm(data);
  domID("tknv").disabled = true;
  domID("btnCapNhat").style.display = "block";
  domID("btnThemNV").style.display = "none";
  resetThongBao(mangId);
}

///

///
document.getElementById("btnCapNhat").onclick = function () {
  var nhanVien = layThongTinTuForm();
  var viTri = timViTri(nhanVien.taiKhoan, danhSachNhanVien);
  var isValid = true;
  // kiem tra tai khoan
  isValid =
    isValid &
    (kiemTraRong(nhanVien.taiKhoan, mangId[0], mangThongBao[0]) &&
      kiemTraDoDai(nhanVien.taiKhoan, mangId[0], 4, 6));
  //kiem tra ten
  isValid =
    isValid &
    (kiemTraRong(nhanVien.hoVaTen, mangId[1], mangThongBao[1]) &&
      kiemTraKiTuNhap(nhanVien.hoVaTen, regexKTT, mangId[1], mangThongBao[10]));
  // kiem tra email
  isValid =
    isValid &
    (kiemTraRong(nhanVien.email, mangId[2], mangThongBao[2]) &&
      kiemTraKiTuNhap(nhanVien.email, regexEmail, mangId[2], mangThongBao[11]));
  // kiem tra mat khau
  isValid =
    isValid &
    (kiemTraRong(nhanVien.matKhau, mangId[3], mangThongBao[3]) &&
      kiemTraKiTuNhap(
        nhanVien.matKhau,
        regexKTMK,
        mangId[3],
        mangThongBao[12]
      ));
  //kiem tra ngay
  isValid =
    isValid &
    (kiemTraRong(nhanVien.ngayLam, mangId[4], mangThongBao[4]) &&
      kiemTraKiTuNhap(
        nhanVien.ngayLam,
        regexKTNgay,
        mangId[4],
        mangThongBao[13]
      ));
  isValid =
    isValid &
    (kiemTraRong(nhanVien.luongCoBan, mangId[5], mangThongBao[5]) &&
      kiemTraKiTuNhap(
        nhanVien.luongCoBan,
        regexKTS,
        mangId[5],
        mangThongBao[9]
      ) &&
      kiemTraNhapSo(
        1000000,
        nhanVien.luongCoBan,
        mangId[5],
        mangThongBao[14],
        20000000
      ));
  isValid = isValid & kiemTraChucVu(mangId[7], mangThongBao[6]);
  isValid =
    isValid &
    (kiemTraRong(nhanVien.soGioLam, mangId[6], mangThongBao[7]) &&
      kiemTraKiTuNhap(
        nhanVien.soGioLam,
        regexKTS,
        mangId[6],
        mangThongBao[9]
      ) &&
      kiemTraNhapSo(80, nhanVien.soGioLam, mangId[6], mangThongBao[15], 200));
  if (isValid) {
    danhSachNhanVien[viTri] = nhanVien;
    luuLocalstorage();
    renderDSNV(danhSachNhanVien);
  }
};
document.getElementById("btnThem").onclick = function () {
  resetForm();
  document.getElementById("tknv").disabled = false;
  document.getElementById("btnCapNhat").style.display = "none";
  document.getElementById("btnThemNV").style.display = "block";
  resetThongBao(mangId);
};

document
  .getElementById("searchName")
  .addEventListener("keypress", function (event) {
    if (event.key === "Enter") {
      event.preventDefault();
      document.getElementById("btnTimNV").click();
    }
  });

document.getElementById("btnTimNV").onclick = function () {
  var userNhap = document.getElementById("searchName").value;
  //regex
  var regexXS = /xuất sắc|xuatas satws|xuat61 sac81|x|xs/g;
  var regexTB =
    /trungbinh|trung bình|tb|trb|trungbinhf|trung bình|trung binh2|t|b/g;
  var regexKha = /khas|kha|k|kh|khá|nhanvienkha|nhân viên khá/g;
  var regexG = /giỏi|gioir|g|gi|gioi|nhanviengioi|Nhân viên giỏi|nhânviengioi/g;
  //danh sach nhan vien xep loai
  var dsnvTB = timNV(danhSachNhanVien, "Nhân viên trung bình");
  var dsnvXS = timNV(danhSachNhanVien, "Nhân viên xuất sắc");
  var dsnvG = timNV(danhSachNhanVien, "Nhân viên giỏi");
  var dsnvK = timNV(danhSachNhanVien, "Nhân viên khá");
  if (userNhap.match(regexXS)) {
    renderDSNV(dsnvXS);
  } else if (userNhap.match(regexTB)) {
    renderDSNV(dsnvTB);
  } else if (userNhap.match(regexG)) {
    renderDSNV(dsnvG);
  } else if (userNhap.match(regexKha)) {
    renderDSNV(dsnvK);
  } else {
    renderDSNV(danhSachNhanVien);
  }
};
