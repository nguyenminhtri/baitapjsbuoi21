function domID(id) {
  return document.getElementById(id);
}

function kiemTraTrung(id, dsnv) {
  var index = timViTri(id, dsnv);
  if (index !== -1) {
    domID("tbTKNV").innerHTML = "Tài khoản đã tồn tại";
    domID("tbTKNV").style.display = "block";
    return false;
  } else {
    domID("tbTKNV").innerHTML = "";
    return true;
  }
}
//kiem tra rong
function kiemTraRong(userNhap, id, thongBao) {
  if (userNhap == "") {
    domID(id).innerHTML = thongBao;
    domID(id).style.display = "block";
    return false;
  } else {
    return true;
  }
}

//kiem tra ki tu nhap
function kiemTraKiTuNhap(userNhap, regex, id, thongBao) {
  if (regex.test(userNhap) == false) {
    domID(id).innerHTML = thongBao;
    domID(id).style.display = "block";
    return false;
  } else {
    domID(id).style.display = "none";
    return true;
  }
}

function kiemTraChucVu(id, thongBao) {
  var theSelect = domID("chucvu");

  if (theSelect.selectedIndex == 0) {
    domID(id).innerHTML = thongBao;
    domID(id).style.display = "block";
    return false;
  } else {
    domID(id).style.display = "none";
    return true;
  }
}
function kiemTraDoDai(userNhap, id, minlength, maxlength) {
  if (userNhap.length >= minlength && userNhap.length <= maxlength) {
    domID(id).style.display = "none";
    return true;
  } else {
    domID(id).innerHTML =
      "Độ dài kí tự tối thiểu, tối đa " + minlength + " " + maxlength;
    domID(id).style.display = "block";
    return false;
  }
}
function kiemTraNhapSo(dk1, userNhap, id, thongBao, dk2) {
  if (dk1 <= userNhap && userNhap <= dk2) {
    domID(id).style.display = "none";
    return true;
  } else {
    domID(id).innerHTML = thongBao;
    domID(id).style.display = "block";
    return false;
  }
}

function resetThongBao(mangId) {
  for (i = 0; i < mangId.length; i++) {
    var id = mangId[i];
    domID(id).style.display = "none";
  }
}
// regex kiem tra ki tu
var regexKTKT = /^[A-Za-z]+$/;
// kiem tra so
var regexKTS = /^[0-9]+$/;
//
var regexKTT = /^[a-zA-Z ]+$/;
//
var regexKTMK =
  /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{6,10}$/;
var regexEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
//kiem tra ngay
var regexKTNgay =
  /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;
